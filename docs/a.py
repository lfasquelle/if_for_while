def pepg(m):
    """
    m -- entier naturel
    
    renvoie le plus petit entier naturel n tel que 2^n > m.
    >>> pepg(0)
    0
    >>> pepg(1)
    1
    >>> pepg(2)
    2
    >>> pepg(3)
    2
    >>> pepg(4)
    3
    >>> pepg(5)
    3
    """
    p = 1
    n = 0
    while p <  m:
        p = p*2
        n = n+1
    return n 
    
    
def test_pepg():
    """
    Teste la fonction pepg.
    La fonction stoppe à la première erreur et renvoie un message
    à l'utilisateur.
    En cas d'absence d'erreur, la fonction renvoie le message "OK".
    """
    if pepg(0) != 0: 
        return f"Erreur: pepg({0}) = {pepg(0)}."
    for m in range(1, 100):
        n = pepg(m)
        if not(2**n > m):
            return f"pepg({m}) = {n}. Erreur: on n'a pas 2**pepg({m}) > {m}."
        if 2**(n-1) > m:
            return f"pepg({m}) = {n}. Erreur: on  a   2**(pepg({m})-1) > {m}."
    return "OK"
    
 
print(test_pepg())
        
