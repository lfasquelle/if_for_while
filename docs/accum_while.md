# Accumulateur et while

 



## Exercice 


&Eacute;crire une fonction python prenant en entrée un entier naturel M 
et renvoyant en sortie le plus grand entier naturel $n$ tel que $0+1+2+\dots+n \leqslant M$. 


```
>>> fin_auplus(2)
1
>>> fin_auplus(3)
2
>>> fin_auplus(4)
2
>>> fin_auplus(5)
2
>>> fin_auplus(6)
3
>>> fin_auplus(9)
3
>>> fin_auplus(10)
4
```


??? solution "Solution: un code"

    ```python
    def fin_auplus(M):
        """
        M -- entier naturel

        renvoie le plus grand entier n tel que
        0+1+2+...+n <= M
        """
        i = 0
        S = 0
        while S <= M:
            i += 1
            S += i
        return i-1
    ```




## Exercice 

On reprend l'exercice de la page précédente:

&Eacute;crire une fonction prenant en entrée un entier naturel A non nul et
renvoyant le plus petit entier m tel que $2^m > A$.

   
Mais il s'agit mainteant d'écrire une version dans laquelle on s'interdira d'utiliser ** pour calculer une puissance.



??? solution "Indication: accumulateur"

    L'idée, pour calculer les puissances de 2 successives sans utiliser **, est à nouveau celle de l'accumulateur.
    
    Le nombre $2^n$ est le produit de 
    2, 2, 2, ..., 2:  
    2<sup>n</sup> = 2&times;2&times;2&times;...&times;2 peut se calculer en "accumulant" les facteurs 2...
    
    
    On commence par:
    
    P ← 1
    
    Puis on multiplie par 2 à chaque étape le contenu de P:
    
    P ← 2P  # P désigne maintenant la valeur 2  
    P ← 2P  # P désigne maintenant la valeur 2 &times; 2 = 2<sup>2</sup>  
    P ← 2P  # P désigne maintenant la valeur 2 &times; 2<sup>2</sup>   = 2<sup>3</sup>  
    P ← 2P  # P désigne maintenant la valeur 2 &times; 2<sup>3</sup>   = 2<sup>4</sup>   
    P ← 2P  # P désigne maintenant la valeur 2 &times; 2<sup>4</sup>   = 2<sup>5</sup>   
    ....
    
    
??? solution "Un code"


    ```python
    def depasseA(A):
        """
        A -- entier naturel non nul
        
        renvoie  le plus petit entier n tel que 2**n > A
        """
        n = 0
        p = 1   # on p = 1 = 2^0 = 2^n
        while p <= A:
            n += 1
            p = 2*p   # p = 2^n à chaque fin de passage dans la boucle
        
        return n 
    ```
    
    
