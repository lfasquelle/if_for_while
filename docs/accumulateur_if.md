# if et accumulateur

 
  




##  Exercice

&Eacute;crire une fonction prenant en paramètre un entier naturel n et renvoyant en sortie la somme
des entiers **pairs** compris (au sens large) entre  0 à n: S = 0 + 2 + 4 + ... (le dernier terme étant n si n est pair
et n-1 sinon).


??? solution "Indication: reconnaître un pair"

    n désignant un entier, le test `n%2 == 0` est égal à `True`  si et seulement si n est pair.

??? solution "Le principe"

    Il s'agit d'appliquer le principe d'accumulation déjà utilisé.
    
    On crée une variable S initialisée à 0 dans laquelle on va ajouter (accumuler) un à un tous les termes 
    pairs.
    
   
    ``` 
    S ← 0
    S ← S + 2 # S désigne maintenant la valeur 2
    S ← S + 4 # S désigne maintenant la valeur 2+4
    S ← S + 6 # S désigne maintenant la valeur 2+4+6
    S ← S + 8 # S désigne maintenant la valeur 2+4+6+8
    S ← S + 10 # S désigne maintenant la valeur 2+4+6+8+10
    ...
    ```
     
    
    A vous d'écrire cela à l'aide d'une fonction python.


??? solution "Un code"

    ```python
    def somme_pairs(n):
        """
        n -- entier naturel

        renvoie la somme  des entiers naturels pairs
        valant au plus n.
        """
        S = 0
        for i in range(1, n+1):
            if i%2 == 0:
                S = S + i
        return S
    ```
    
    
    
    
    
     
    Tests:
    
    ```
    >>> somme_pairs(4)
    6
    >>> somme_pairs(5)
    6
    >>> somme_pairs(6)
    12
    >>> somme_pairs(8)
    20
    ```

 
 
## Exercice

&Eacute;crire une fonction prenant en paramètre un entier naturel n et renvoyant en sortie la somme
des entiers **multiples de trois** compris (au sens large) entre  0 à n.

```
>>> somme_multiple3(3)
3
>>> somme_multiple3(5)
3
>>> somme_multiple3(9)
18
```
 
    
??? solution "Solution: un code"


    ```python
    def somme_multiple3(n):
        """
        n -- entier naturel

        renvoie la somme  des entiers naturels multiples de 3
        valant au plus n.
        """
        S = 0
        for i in range(1, n+1):
            if i%3 == 0:
                S = S + i
        return S
    ```
    
    
