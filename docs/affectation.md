# Affectation d'une variable


Avant d'aborder la page suivante sur la portée des variables, on rappelle ici qu'en python, on peut voir
voir deux types d'affectation différents.

+ l'affectation avec calcul dans la partie droite de l'affectation.
+ l'affectation avec une simple étiquette déjà existante dans la partie droite de l'affectation. On appellera 
affectation d'étiquette ce type d'affectation.


## Algorithme

Lorsque nous décrirons les algorithmes en pseudo-code, nous utiliserons la syntaxe suivante:

```
a ← 3
```

qui signifiera que la variable a reçoit la valeur 3.

Rappelons que dans l'écriture suivante:

```
a ← 3
a ← a + 6
```

on ne peut comprendre la seconde ligne qu'en se rappelant que l'on commence toujours par l'expression qui 
se trouve à droite de la flèche.

+ En ligne 1, la variable a reçoit la valeur 3.
+ En ligne 2, on effectue la partie droite: on calcule donc 3+6 = 9. Puis on affecte cette valeur 9 à a. 
a vaut ainsi 9 après la seconde ligne.


## En python

 

En python, affecter la valeur 3 à la variable `a` se code comme suit:

```python
a = 3
```

Python crée alors un objet de type int dont la valeur est 3.

Nous schématiserons cela ainsi: 

![](affectation/affectation.png)



L'opération d'affectation `a ← a + 6` se code en python comme suit:

```python
a = a + 6
```

Ici encore, bien se souvenir que la lecture se fait en commençant par l'opération de droite.

On représentera le code:

```python
a = 3
a = a + 6
```

comme suit:

+ En ligne 1, création d'un objet de type int recevant la valeur 3 et recevant une "étiquette" (un nom) `a`:   
![](affectation/affectation.png)
+ En ligne 2, création d'un nouvel objet de type int recevant la valeur 3+6 puis recevant  l'étiquette `a`:  
![](affectation/affectation2.png)


Il sera important dans la suite de comprendre que toute affectation crée un nouvel objet (une nouvelle boîte dans
notre représentation): ce n'est pas la première boîte qui reçoit une nouvelle valeur. 

La première boîte existe toujours a priori mais c'est comme si elle n'existait plus: elle n'a plus d'étiquette, 
elle n'est donc plus accessible pour le programmeur.

Si nous avions exécuté le code suivant:

```python
a = 3
b = a + 6
```

Le schéma aurait été:

+ En ligne 1, création d'un objet de type int recevant la valeur 3 et recevant une "étiquette" (un nom) `a`:   
![](affectation/affectation.png)
+ En ligne 2, création d'un nouvel objet de type int recevant la valeur 3+6 puis recevant  l'étiquette `b`:  
![](affectation/affectation3.png)

Dans ce cas, les deux boîtes ont toujours une étiquette: elles sont encore toutes deux accessibles.


## Affectation d'étiquette

En python, lorsqu'il n'y a pas de calcul dans la partie droite de l'affectation, il n'y aura pas cette fois de création 
d'un nouvel objet mais simple ajout d'une étiquette à l'objet.

Le code:

```python
a = 3
b = a
```

se schématisera ainsi:

+ Ligne 1: création d'un objet de type int recevant la valeur 3 et l'étiquette a.  
![](affectation/affectation.png)
+ Ligne 2: ajout d'une étiquette b sur l'objet existant.  
![](affectation/etiquette.png)

Nous  verrons que ce comportement sera important à comprendre pour des objets plus complexes 
que les int et les float (pour les chaînes, les listes, les tuples notamment).
