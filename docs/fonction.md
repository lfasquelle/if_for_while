# Les fonctions



## Syntaxe


Toutes vos fonctions devront se présenter sous la forme générique suivante (le nombre 
de paramètres peut bien sûr être différent de 2):

```
def nomFonction(paramètre1, paramètre 2):
    """
    paramètre1 -- type du paramètre, précondition éventuelle
    paramètre2 -- type du paramètre, précondition éventuelle
    
    renvoie...(type de ce qui est renvoyé, liens avec les paramètres)
    """
    CORPS de la fonction
```

Le nom choisi pour la fonction doit être explicite (c'est à dire correspondre à son rôle).  
Il en va de même pour tout nom de variable dans vos programmes.

## Chaîne de documentation

La partie entre """ et """ qui suit l'entête et précéde le code du corps est nommé **docstring** (*chaîne de documentation*).

Toutes les fonctions que vous écrirez (notamment celles qui seront rendues à l'enseignant) devront comporter
une chaîne de documentation.

Ce docstring présente au moins la forme minimale donnée ci-dessus, il spécifie la fonction:

+ décrit les paramètres en donnant leur type, 
+ donne les préconditions éventuelles sur ces paramètres,
+ donne le type de la sortie, ainsi que son lien avec les paramètres.


## Paramètres


Dans le code ci-dessous, 

```
def nomFonction(paramètre1, paramètre 2):
    """
    paramètre1 -- type du paramètre, précondition éventuelle
    paramètre2 -- type du paramètre, précondition éventuelle
    
    renvoie...(type de ce qui est renvoyé, liens avec les paramètres)
    """
    CORPS de la fonction
```

on a utilisé le terme **paramètre**. 

On distingue parfois les expressions **paramètre formel** et **paramètre effectif**.

+ "paramètre formel" est utilisé lors de la définition du code de la fonction.
+ "paramètre effectif" désigne une valeur utilisée lors d'un appel de la fonction.

A la place de "paramètre effectif", on utilise également le terme **argument**, ce qui permet 
d'utiliser, sans ambiguïté, le terme paramètre pour désigner un paramètre formel.

??? note "Exemple"

    Dans le code:
    
    ```python
    def carre(x):
        return x*x
    ```
    
    Utilisation: 
    
    ```
    >>> carre(5)
    >>> v = 7
    >>> carre(v)
    ```
    
    + le paramètre formel est `x`.
    + Lors des appels, on a utilisé les paramètres effectifs (ou arguments) 5 puis v (v désignant la valeur 7).

## help


Ouvrez l'interpréteur python3 en console et entrez:

```
>>> from math import sqrt
>>> help(sqrt)
```

Vous devriez obtenir:

```
Help on built-in function sqrt in module math:

sqrt(x, /)
    Return the square root of x.
(END)
```
(vous pouvez normalement quitter en entrant q)

Ce qui est affiché en réponse de votre commande `help(sqrt)` est le docstring de la fonction sqrt.


### A vous

&Eacute;crire une fonction f dans un fichier a.py, avec son docstring.
Exécuter ensuite `help(f)` depuis l'interpréteur python3 ouvert en console.



??? solution "Un exemple"

    Définissons dans le fichier a.py:
    
    ```python
    def carre(x):
        """
        x -- flottant
        
        renvoie le flottant carré de x
        """
        return x*x    
    ```

    Dans une console (ouverte dans le dossier de notre script), on entre python3
    puis:
    
    ```
    >>> from a import carre
    >>> help(carre)
    ```

    On obtient:
    
    ```
    Help on function carre in module a:

    carre(x)
        x -- flottant
        
        renvoie le flottant carré de x
    (END)
    ```



 
