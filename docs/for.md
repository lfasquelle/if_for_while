# for


La boucle for permet de parcourir les valeurs d'une séquence (ou les valeurs créées par un itérateur).


## Parcourir les caractères d'une chaîne de caractères.

Une chaîne de caractères est délimitée par des guillemets.

```
>>> ch = "Coucou, bouh, zou."
>>> for caractere in ch:
...     print(caractere)
... 
C
o
u
c
o
u
,
 
b
o
u
h
,
 
z
o
u
.
```


## Parcourir les éléments d'une liste.

Une liste est un ensemble d'éléments séparés par des virgules et encapsulés par des crochets.

```
>>> a = [1, 4, 8, 2, 42, 666]
>>> for element in a:
...     print(element)
... 
1
4
8
2
42
666
```


## Parcourir les éléments d'un tuple.

Un tuple est un ensemble d'éléments séparés par des virgules et encapsulés par des parenthèses.

```
>>> a = (1, 4, 8, 2, 42, 666)
>>> for element in a:
...     print(element)
... 
1
4
8
2
42
666
```


!!! note
    Ne vous inquiétez pas pour le moment de quelle peut être la différence entre liste et tuple.
    Nous y reviendrons: un chapitre sera consacré aux tuples, un chapitre sera consacré aux listes.

## Parcourir les valeurs créées par un itérateur


L'exemple le plus usuel est celui de `range`.

On rappelle que, a et b désignant deux entiers, `range(a,b)` permet de parcourir une suite d'entiers consécutifs.
Cette suite commence à $a$ et compte $b-a$ éléments (et se termine donc à $b-1$).   

`range(b)` est une abréviation pour `range(0,b)` et permet donc de parcourir une suite d'entiers consécutifs
de longueur $b$ et commençant à 0 (se terminant donc en $b-1$).


```
>>> for i in range(5):
...     print(i)
... 
0
1
2
3
4
```

On peut également utiliser `range(a, b, p)` pour parcourir les valeurs a, a+p, a+2p, a+3p, ..., a+kp (où
a+kp est la dernière valeur s'écrivant sous cette forme tout en restant strictement inférieure à b).



 
