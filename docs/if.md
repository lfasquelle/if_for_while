# if


## La syntaxe

```
if condition:
    actions à exécuter si condition est satisfaite
else:
    actions à exécuter si condition n'est pas satisfaite.
```


## Exercice

&Eacute;crire un code pour le corps de la fonction suivante:

```python
def f(n):
    """
    n -- entier

    renvoie le triple de n si n est pair, le double de n si n est impair.
    
    >>> f(4)
    12
    >>> f(5)
    10
    """
```


!!! note "Multiple de"

    On rappelle que, pour a et b entiers,  `a%b` est le reste de la division entière de a par b.
    
    Le test `a%b == 0` signifie donc "le reste de la division entière de a par b est nul", ou en d'autres 
    termes: "a est multiple de b".
    
    Cette remarque sera souvent utilisée... A retenir!


??? solution "Un code possible"

    

    ```python
    def f(n):
        """
        n -- entier

        renvoie le triple de n si n est pair, le double de n si n est impair.
        
        >>> f(4)
        12
        >>> f(5)
        10
        """
        if n%2 == 0:
            return 3*n
        else:
            return 2*n
    ```


!!! attention "Algorithme de travail des exercices"

    Rappelons que vous ne devez **jamais** vous contenter de lire la solution. 
    Pour chaque exercice, votre démarche doit être la même:
    
    
        ok ← faux
        Tant que non ok:
            * Cherchez par vous-même, testez, cherchez, testez, cherchez, testez, cherchez...
            
            * Si vous êtes parvenu à écrire un code qui semble fonctionner:
                - vous lisez la correction,
                - vous essayez de comprendre les éventuelles divergences entre votre code 
                et la correction proposée.
                - ok ← vrai
                
             sinon:
                - vous lisez la solution,  
                - vous la fermez ensuite et vous essayez à nouveau 
                (pas de copier-coller, c'est totalement inutile!)
                (et ok vaut toujours faux! donc retour en début de boucle !!!)
    
    
      Et dans tous les cas, cette étape doit être recommencée jusqu'à ce que vous arriviez 
      par vous-même à écrire un code sans erreur.... Et dans les deux jours qui suivent, on recommence...
      et en fin de semaine, pour ancrer tout cela, on recommence.
