# Indenter



## Définition

Dans beaucoup de langages, on délimite des blocs par des accolades. Et tout bon programmeur utilise
de façon systématique l'indentation pour la lisibilité de son code.

!!! note "indentation"

    Extrait de [wikipedia](https://fr.wikipedia.org/wiki/Indentation):
    
    En informatique : l'indentation consiste en l'ajout de tabulations ou d'espaces dans un fichier, 
    pour une meilleure lecture et compréhension du code.
    
    
Quelques langages, dont Python, ont fait de l'indentation une règle absolue. Ainsi, la lisibilité du code 
est assurée (le code ne fonctionnera pas sans l'indentation convenable), et les accolades deviennent inutiles:
**un bloc est un ensemble de lignes de code présentant la même indentation**.

Exemple:

```
for i in range(n):
    ligne 1 du bloc for
    ligne 2 du bloc for
    ligne 3 du bloc for
ligne en-dehors du bloc for car "désindenté"
```

Cet exemple vaut également pour délimiter un bloc suivant un if, un else, un while, ou encore le bloc constituant 
le corps d'une fonction.


## L'usage

La règle d'usage en python est d'indenter de quatre espaces pour marquer un bloc (en appuyant quatre fois
sur la barre d'espace).


## Les éditeurs de texte

Lorsqu'on utilise un éditeur de texte pour taper son code python:

+ s'il est adapté (par exemple geany ou spyder), dès que l'on entre les deux-points terminant une ligne for (ou if ou while
ou def), et que l'on passe à la ligne, l'éditeur fait l'indentation automatiquement.
+ on peut aussi à la place des 4 appuis sur la barre d'espace, appuyer une seule fois sur la touche de tabulation.


**Attention** toutefois, **on ne mélange pas espaces et tabulations**.
Ce mélange d'espaces et de tabulations est très souvent une cause d'erreur.
Notamment lorsque vous travaillez à la maison sur un autre éditeur que celui du lycée.
**Règlez systématiquement votre éditeur de texte pour qu'un appui sur la touche de tabulation ait en fait pour effet 
de placer 4 espaces**.

Prenez l'habitude également de faire afficher les espaces dans votre éditeur. Vous verrez ainsi plus facilement les erreurs
dues à un changement d'indentation (sans les faire afficher, il n'est pas possible de faire la différence visuellement
entre 4 espaces et une touche de tabulation non parémétrée pour écrire 4 espaces).

!!! note "avec Geany"
    Pour qu'un appui sur la tabulation génére 4 espaces (et non une tabulation) sur le logiciel geany,
    il suffit de sélectionner Document/type d'indentation/Espaces.
    
!!! note "avec Geany"
    Pour afficher les espaces: Affichage/Afficher les espaces
    

!!! note "Avec Spyder"
    La tabulation est normalement paramétrée par défaut pour inscrire 4 espaces (et non une tabulation).    
    
!!! note "Avec Spyder"
    Pour afficher les espaces: Source/Afficher les espaces.
    

!!! important 
    Tous les programmes que vous devrez rendre devront au minimum ne pas comporter d'erreur de syntaxe.
    En particulier, toute erreur d'indentation laissée dans votre code donnera lieu à la note minimale.
