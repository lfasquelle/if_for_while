# for, if, while et fonctions





Vous allez ici réviser les structures vues en seconde, via la présentation de quelques compléments.

Toutes les notions relatives à for, if, while, def, return et le vocabulaire associé doivent être 
maîtrisés au plus vite, cela relève du programme de la classe de seconde. 

Quelques notions complémentaires introduites ici (chaînes de caractères, liste, tuple, tests, docstrings) 
seront   travaillées tout au long de l'année.
 

 
 



