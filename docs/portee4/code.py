a = 2

def f():
    b = a + 3
    return b

print("Variable a avant appel de f:", a)
f()
print("Variable a après appel de f:", a)
