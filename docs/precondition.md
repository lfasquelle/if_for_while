# Conditions sur les paramètres


 

## Précondition

Nous avons parlé de précondition ci-dessus. Il s'agit de conditions à respecter par les paramètres 
pour que le calcul effectué par la fonction ait un sens.

Par exemple, pour une fonction racine carrée `racineCarree(x)`, 
le paramètre x est un flottant et l'on imposera la précondition
$x \geqslant 0$.

```
def racineCarree(x):
    """
    x -- flottant
    précondition: x >= 0
    
    renvoie le flottant racine carrée de x
    """
```


## Précondition dans le code

On peut également renforcer une précondition dans le code à l'aide d'un `assert`.

La syntaxe:

```
assert condition à respecter, "Message d'alerte à l'utilisateur en cas de non respect de la précondition"
```

Exemple:

```python
def racineCarree(x):
    """
    x -- flottant
    précondition:  x >= 0
    
    renvoie le flottant racine carrée de x
    """
    assert x >= 0, "Le paramètre x doit être positif ou nul."
    return x**0.5
```

### A vous 

Tester la fonction racineCarree proposée avec une valeur de x négative.
Puis tester avec des valeurs positives.


### A vous

&Eacute;crire une fonction prenant en paramètres quatre entiers a, b, c, d et renvoyant en sortie True
si la fraction $\frac{a}{b}$ est égale à la fraction $\frac{c}{d}$ et renvoie False sinon.

Quelle précondition semble-t-il raisonnable d'imposer? Utiliser un assert pour le traduire dans le code.


??? note "Attention"
    Le code `if a/b == c/d` serait une bien mauvaise idée. Rappeler vous: on ne peut pas comparer des flottants!


??? solution "Un code"

    ```python
    def fractionsEgales(a, b, c, d):
        """
        a -- entier naturel
        b -- entier naturel 
        précondtion: b non nul
        c -- entier naturel
        d -- entier naturel  
        précondition: d non nul
        """
        assert b != 0, "Attention, le dénominateur b ne peut être nul."
        assert d != 0, "Attention, le dénominateur d ne peut être nul."
        return a*d == b*c
    ```

 
