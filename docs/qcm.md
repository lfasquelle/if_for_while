#  QCM




## QCM 1

  
Antoine doit écrire une fonction qui prend en paramètre un entier n et renvoie True si cet entier est pair, 
et False si cet entier est impair.

Il écrit le code suivant:

```python
def f(n):
    if n%2 == 0:
        return True
    else:
        return False
```

Que peut-on reprocher à ce code:

- [ ] la fonction est mal nommée.
- [ ] il manque le docstring.
- [ ] le corps est mal écrit.


??? solution "Réponse"

    - [X] la fonction est mal nommée.
    - [X] il manque le docstring.
    - [X] le corps est mal écrit.
    
    + La fonction est mal nommée. Nommer toujours explicitement une fonction par un nom rappelant son rôle.
    On pourra appeler cette fonction `estPair` par exemple.
    + Il manque effectivement le docstring, indispensable. 
    + Le code de cette fonction sera  plus lisible sans instruction inutile:
    
    ```python
    def estPair(n):
        """ 
        n -- entier 
        
        renvoie True si n est pair, False si n est impair.
        >>> estPair(0)
        True
        >>> estPair(1)
        False
        >>> estPair(100)
        True
        >>> estPair(2013)
        False
        """
        return n%2 == 0
    ```
    
    
## QCM 2

On considère la fonction suivante, le paramètre n désignant un entier strictement positif.

```python
def f(n):
    p = 1
    for k in range(1,n+1):
        p = p*k
    return p
```
 
 
Que renvoie l'interpréteur python:

```
>>> f(4)

```

- [ ] 6
- [ ] 24
- [ ] 1
- [ ] une erreur

??? solution "Réponse"

    - [ ] 6
    - [X] 24
    - [ ] 1
    - [ ] une erreur
    
    Complétons le docstring de f et ajoutons une assertion pour contrôler la précondition sur n:
    
    ```python
    def f(n):
        """
        n -- entier naturel non nul
        
        renvoie le produit des entiers de 1 à n: 1*2*3*...*n.
        >>> f(0)
        Attention, le paramètre doit être un entier >0.
        >>> f(1)
        1
        >>> f(2)
        2
        >>> f(3)
        6
        >>> f(9)
        362880
        """
        assert isinstance(n, int) and n > 0, "Attention, le paramètre doit être un entier >0."
        p = 1
        for k in range(1,n+1):
            p = p*k
        return p
    ```

     




## QCM 3

On considère la fonction suivante:

```
def g(n):
    s = 0
    for i in range(0, n, 2):
        s = s+i
    return s
```

Avec:

```
>>> g(10)
```

on obtient:


- [ ] 30
- [ ] 20
- [ ] 45
- [ ] 55

??? solution "Réponse"

    - [ ] 30
    - [X] 20
    - [ ] 45
    - [ ] 55
    
    
    ```python
    def g(n):
        """
        n -- entier naturel
        
        renvoie la somme des entiers naturels pairs 
        strictement inférieurs à n.
        >>> g(0)
        0
        >>> g(1)
        0
        >>> g(2)
        0
        >>> g(3)
        2
        >>> g(4)
        2
        >>> g(5)
        6
        """
        s = 0
        for i in range(0, n, 2):
            s = s+i
        return s
    ```
    
    
## QCM 4


On considère le code python:

```python
def f(a,b):
    """
    a -- entier naturel
    b -- entier naturel non nul
    """
    q = 0
    r = a
    while r >= b:
        r = r-b
        q = q+1
    return q, r
```

Qu'obtient-on avec l'appel:

```
>>> f(7,2)
```

- [ ] (3, 1)
- [ ] (2, 3)
- [ ] (4, -1)
- [ ] une erreur



??? solution "Réponse"

    - [X] (3, 1)
    - [ ] (2, 3)
    - [ ] (4, -1)
    - [ ] une erreur
    
    
    f pourrait être renommée  "division": f(a,b) est le couple (quotient, reste)
    de la division entière de a par b.




   
## QCM 5


On considère le code python:

```python
def division(a,b):
    """
    a -- entier naturel
    b -- entier naturel non nul
    
    renvoie le couple (quotient, reste) de la division entière de a par b.
    """
    q = 0
    r = a
    while r >= b:
        r = r-b
        q = q+1
    return q, r
```

On veut tester cette fonction.

Quel jeu de tests est le plus approprié:

- [ ] division(2,9), division(11,2), division(91,9), division(10,4)
- [ ] division(9,2), division(1,1), division(91,91), division(10,4)
- [ ] division(0,7), division(2,8), division(9,9), division(10,4)


??? solution "Réponse"



    - [ ] division(2,9), division(11,2), division(91,9), division(10,4)
    - [ ] division(9,2), division(1,1), division(91,91), division(10,4)
    - [X] division(0,7), division(2,8), division(9,9), division(10,4)
    
    En présence d'une boucle while, on essaie de tester:
    
    + au moins un cas où l'on n'entre pas dans la boucle (division(0,7), division(2,8)),
    + au moins un cas d'égalité pour la comparaison du while (division(9,9)),
    + au moins un cas "générique" (on entre dans la boucle avec r > b, ici division(10,4)).
    
    Pour être complet ici, on pourrait ajouter des assertions pour contrôler le respect des 
    préconditions sur a et b:
    
    ```python
    def division(a,b):
        """
        a -- entier naturel
        b -- entier naturel non nul
        
        renvoie le couple (quotient, reste) de la division entière de a par b.
        >>> division(0,7)
        (0, 0)
        >>> division(2,7)
        (0, 2)
        >>> division(9,9)
        (1, 0)
        >>> division(10,4)
        (2, 2)
        """
        assert isinstance(a, int) and a >= 0, "Attention a doit être un entier positif ou nul."
        assert isinstance(b, int) and b > 0, "Attention b doit être un entier strictement positif."
        q = 0
        r = a
        while r >= b:
            r = r-b
            q = q+1
        return q, r
    ```


## QCM 6

Avec le code suivant:
 

```python
def g(x):
    a = a + x
    return a

a = 5
print(g(2))
```

on obtient:

- [ ] une erreur
- [ ] l'affichage de la valeur 7

??? solution "Réponse"

    - [X] une erreur
    - [ ] l'affichage de la valeur 7
 
    La présence d'une affectation `a =  a+x` fait de `a` une variable locale. De ce fait, on essaie
    d'augmenter de 2 la valeur de `a` alors que `a` n'a pas de valeur affectée.
    On obtient donc l'erreur `local variable 'a' referenced before assignment`.

 
    
    
    
    
    
    

## QCM 7

Avec le code suivant:

```python
def g(x):
    y = a + x
    return y

a = 5
print(g(2))
```

on obtient:

- [ ] une erreur
- [ ] l'affichage de la valeur 7

??? solution "Réponse"

    - [ ] une erreur
    - [X] l'affichage de la valeur 7
    
    Cette fois, il n'y a pas d'affectation sur `a`, `a` désigne donc la variable globale
    et avec `y = a + x`, y reçoit la valeur 7.

