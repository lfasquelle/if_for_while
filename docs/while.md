# while


## La syntaxe


```
while condition:
    actions à exécuter tant que condition est satisfaite
instructions (éventuelles) à exécuter après la boucle while
```

La ligne 3 est en-dehors de la boucle, 
elle est exécutée lorsque condition n'est plus satisfaite, c'est à dire lorsque non(condition)
est satisfaite.



 
## Exercice

+ &Eacute;crire une fonction prenant en entrée un entier naturel A non nul et
renvoyant le plus petit entier m tel que $2^m > A$.


!!! attention "Comment chercher ?"

    Pour écrire un code, on cherche d'abord simplement à traduire **ce que l'on ferait "à la main"**.
    
    Je cherche par exemple le premier entier n tel $2^n > 13$:
    
    + Je teste 0. $2^0 \leqslant 13$.
    + Je teste l'entier suivant, à savoir 1. $2^1 \leqslant 13$.
    + Je teste l'entier suivant, à savoir 2. $2^2=4$ donc $2^2 \leqslant 13$.
    + Je teste l'entier suivant, à savoir 3. $2^3=8$ donc $2^3 \leqslant 13$.
    + Je teste l'entier suivant, à savoir 4. $2^4=16$ donc $2^4 > 13$.
    
    Le premier entier $n$ tel que $2^n > 13$ est donc $n=4$.
    
    
    J'essaie ensuite de traduire ma démarche à l'aide des instructions d'un langage de programmation.   
    Qu'a-t-on fait ici ? On a testé les entiers un par un **jusqu'à** trouver le bon. 
    Une telle recherche (répéter une action de même nature jusqu'à ce que ...) se traduit toujours en langage python
    par une boucle **while**.
    
    Résumons ce que l'on a fait:
    
    
    On commence avec $n = 0$.   
    Tant que $2^n \leqslant A$, on incrémente $n$.
    
    
    Il ne reste qu'à traduire cela en python.

??? solution "Un code"

    ```python
    def depasseA(A):
        """
        A -- entier naturel non nul

        renvoie  le plus petit entier n tel que 2**n > A
        """
        n = 0
        while 2**n <= A:
            n += 1
        return n 
    ```
    
    On notera bien que la condition dans le while est $\leqslant A$, c'est à dire la négation de $> A$.  
    &Eacute;crire $< A$ dans la condition du while serait une erreur (par exemple depasseA(16) ne donnerait pas
    le même résultat, essayez!)
    
    Exemple d'utilisation avec l'interpréteur python3 en console.
    On suppose la fonction enregistrée dans un fichier a.py:
    ```
    >>> from a import depasseA
    >>> depasseA(9)
    4
    >>> depasseA(16)
    5
    ```
    
    

  
    

    
